<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioFormType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('nombre', TextType::class, 
                [
                    'attr' => ['class' => 'form__input']
                ])
            ->add('email', EmailType::class, 
                [
                    'attr' => ['class' => 'form__input']
                ])
            ->add('ciudad', ChoiceType::class, 
                [
                    'choices' => ['Madrid' => 'Madrid', 'Barcelona' => 'Barcelona', 'Granada' =>'Granada', 'Sevilla' =>'Sevilla', 'Valencia' => 'Valencia'],
                    'attr' => ['class' => 'form__select']
                ])
            ->add('fecha', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
