<?php

namespace App\Form;

use App\Entity\Opinion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpinionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('autor', TextType::class, 
            [
                'attr' => ['class' => 'opinion__input']
            ])
            ->add('comentario', TextareaType::class, 
            [
                'attr' => ['class' => 'opinion__area'],
                'help' => 'Máximo 200 carácteres'
            ])
            ->add('ciudad', ChoiceType::class, 
            [
                'choices' => ['Madrid' => 'Madrid', 'Barcelona' => 'Barcelona', 'Granada' =>'Granada', 'Sevilla' =>'Sevilla', 'Valencia' => 'Valencia'],
                'attr' => ['class' => 'opinion__select']
            ])
            ->add('fecha', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Opinion::class,
        ]);
    }
}
