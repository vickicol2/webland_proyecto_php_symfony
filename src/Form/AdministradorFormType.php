<?php

namespace App\Form;

use App\Entity\Administrador;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdministradorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, 
            [
                'attr' => ['class' => 'admin__input']
            ])
            ->add('email', EmailType::class, 
            [
                'attr' => ['class' => 'admin__input']
            ])
            ->add('usuario', TextType::class, 
            [
                'attr' => ['class' => 'admin__input']
            ])
            ->add('password', PasswordType::class, 
            [
                'attr' => ['class' => 'admin__input']
            ])
            ->add('role', ChoiceType::class, 
            [
                'choices' => ['Administrador' => 'admin', 'Usuario' => 'usuario'],
                'attr' => ['class' => 'admin__select']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Administrador::class,
        ]);
    }
}
