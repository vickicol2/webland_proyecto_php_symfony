<?php

namespace App\Controller;

use App\Entity\Administrador;
use App\Entity\Opinion;
use App\Entity\User;
use App\Entity\Usuario;
use App\Form\AdministradorFormType;
use App\Form\UsuarioFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioController extends AbstractController{

    /**
     * @Route("/webland", name="homepage")
     */
    public function hompepage(Request $request){
        // Request $request para obtener datos del formulario

        /*
            - Crea el formulario de tipo UsuarioFormType, más no lo muestra.
            - Cuando especificamos un formulario de un tipo este toma las propiedades que hay en él para formar los campos, que coinciden también 
              con las propiedades de la clase Entity a la que un formulario debe estar relacionado.
        */
        $formUsuario = $this->createForm(UsuarioFormType::class);

        // Procesamos la petición del formulario
        $formUsuario->handleRequest($request);

        if ($formUsuario->isSubmitted() && $formUsuario->isValid()) {
            // getData() para metodo post
            $usuario = $formUsuario->getData();
            $usuario->setFecha(new \DateTime());
            // dd($usuario->getFecha());
            
            $entityManager = $this->getDoctrine()->getManager();
            
            // Le indicamos a Doctrine que deseamos guardar el artículo (aún no hay consultas)
            
            $entityManager->persist($usuario);

            // Ejecutamos la querie insert
            $entityManager->flush();

            //Enviar un mensaje al usuario
            $this->addFlash('success', 'Gracias! Hemos guardado con éxito tu solicitud de inscripción');

            return $this->redirectToRoute('homepage');
        } else if ($formUsuario->isSubmitted() && !$formUsuario->isValid()) {
            $this->addFlash('error', 'No es válido el Formulario');
            return $this->redirectToRoute('homepage');
        }

        
        /* // Si es una petición ajax
            if( $request->isXmlHttpRequest()) {
                $path = $request->getPathInfo();
                $method = "Si entró";
                $posicionActual = $request->request->get('posicionActual');
            } else {
                $path = 'no hay path';
                $posicionActual = 'no hay pos';
                $method = 'No hay method'; 
            }
            
            //  Obteniendo 3 opiniones aleatorias
            $opiniones = $this->getDoctrine()->getRepository(Opinion::class)->findAll();

            $posiciones = [];

            if (!$opiniones) {
                throw $this->createNotFoundException('No hay opiniones');
            } else {
                $cont = 1;
                while ($cont <= 3){
                    $nuevaPosicion = rand(0, count($opiniones) - 1);

                    if (in_array($nuevaPosicion, $posiciones) == false){
                        $arrOpiniones[] = $opiniones[$nuevaPosicion];
                        $posiciones[] = $nuevaPosicion;
                        $cont++;
                    }
                }
            }

            return $this->render('homepage.html.twig',[
                'formUsuario' => $formUsuario->createView(), 
                'opiniones' => $arrOpiniones,
                'posicionActual' => $posicionActual,
                'path' => $path,
                'method' => $method
            ]);
        
        */
           


        /*  Carousel con 3 opiniones JS
          
            $opiniones = $this->getDoctrine()->getRepository(Opinion::class)->findAll();

            // $posicionActual = 0;
            $total = count($opiniones);
            $NUM_OPINIONES = 3;

            if ($posicionActual <= ($total - $NUM_OPINIONES)){
                $arrOpiniones[] = $opiniones[$posicionActual];
                $arrOpiniones[] = $opiniones[$posicionActual + 1];
                $arrOpiniones[] = $opiniones[$posicionActual + 2];
                $posicionActual++;
            } else {
                $resta = $total - $posicionActual;

                if ($resta > 0){
                    for ($i= 1; $i <= $resta; $i++){
                        $arrOpiniones[] = $opiniones[$posicionActual];
                        $posicionActual++;
                    }
                }

                $posicionActual = 0;

                for ($i= 1; $i <= ($NUM_OPINIONES - $resta); $i++){
                    $arrOpiniones[] = $opiniones[$posicionActual];
                    $posicionActual++;
                }
            }
            
            return $this->render('homepage.html.twig',[
                'formUsuario' => $formUsuario->createView(), 
                'opiniones' => $arrOpiniones,
                'posicionActual' => posicionActual
            ]);
        */
        
        // Paso 3. Opiniones. Envía 3 opiniones aleatorias
        // Consultando las opiniones de la BBDD, devuelve un array
        $opiniones = $this->getDoctrine()->getRepository(Opinion::class)->findAll();

        $posiciones = [];

        if (!$opiniones) {
            throw $this->createNotFoundException('No hay opiniones');
        } else {
            $cont = 1;
            while ($cont <= 3){
                $nuevaPosicion = rand(0, count($opiniones) - 1);

                if (in_array($nuevaPosicion, $posiciones) == false){
                    $arrOpiniones[] = $opiniones[$nuevaPosicion];
                    $posiciones[] = $nuevaPosicion;
                    $cont++;
                }
            }
        }
            
        return $this->render('homepage.html.twig',[
            'formUsuario' => $formUsuario->createView(), 
            'opiniones' => $arrOpiniones
        ]); 
                

    }

    /**
     * @Route("/solicitud/listado", name="solicitudes")
     */
    public function solicitudes(){

        // Consultando las opiniones de la BBDD, devuelve un array
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)->findAll();
                

        if (!$usuarios) {
            throw $this->createNotFoundException('No se han encontrado solicitudes de inscripción');

            return new Response("No solicitudes de inscripción");
        }

        return $this->render('usuario/solicitudes.html.twig', ['usuarios' => $usuarios]);
    }


    /**
    * @Route("/solicitud/delete/{id}", name="eliminar-solicitud")
    * @IsGranted("ROLE_ADMIN")
    */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $solicitud = $entityManager->getRepository(Usuario::class)->find($id);

        if (!$solicitud) {
            throw $this->createNotFoundException('No se ha encontrado esa solicitud');
        }

        // Le indicamos a doctrine que nos gustaría eliminar este elemento, pero no hace la query
        $entityManager->remove($solicitud);
        $entityManager->flush();

      
        return $this->redirectToRoute('solicitudes');
    }

    
    /**
     * @Route("/create")
     * @IsGranted("ROLE_ADMIN")
     */
    public function create(UserPasswordEncoderInterface $pe){
        $user = new User();
        $user->setUsername("jose");
        $user->setPassword($pe->encodePassword($user,"9876"));
        $user->setRoles(['ROLE_READER']);

        $entityManager = $this->getDoctrine()->getManager();
            
        // Le indicamos a Doctrine que deseamos guardar el artículo (aún no hay consultas)
        $entityManager->persist($user);

        // Ejecutamos la querie insert
        $entityManager->flush();
    }


}
