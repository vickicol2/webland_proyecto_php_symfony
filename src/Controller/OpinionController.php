<?php

namespace App\Controller;

use App\Entity\Opinion;
use App\Form\OpinionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

class OpinionController extends AbstractController
{
    /**
     * @Route("/opinion", name="opinion")
     */
    public function opinion(Request $request){
        // Request $request para obtener datos del formulario

        /*
            - Crea el formulario de tipo OpinionFormType, más no lo muestra.
            - Cuando especificamos un formulario de un tipo este toma las propiedades que hay en él para formar los campos, que coinciden también 
              con las propiedades de la clase Entity a la que un formulario debe estar relacionado.
        */
        $formOpinion = $this->createForm(OpinionFormType::class);

        // Procesamos la petición del formulario
        $formOpinion->handleRequest($request);

        if ($formOpinion->isSubmitted() && $formOpinion->isValid()) {
            // getData() para metodo post
            $opinion = $formOpinion->getData();
            $opinion->setFecha(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            
            // Le indicamos a Doctrine que deseamos guardar el artículo (aún no hay consultas)
            $entityManager->persist($opinion);

            // Ejecutamos la querie insert
            $entityManager->flush();

            //Enviar un mensaje al opinion
            $this->addFlash('success', 'Gracias por tu opinión, se ha guardado correctamente!');

            $formOpinion = $this->createForm(OpinionFormType::class);
            return $this->render('opinion/formOpiniones.html.twig',['formOpinion' => $formOpinion->createView(),]);
        } 
        else if ($formOpinion->isSubmitted() && !$formOpinion->isValid()) {
            $this->addFlash('error', 'No es válido el Formulario');
        }


        return $this->render('opinion/formOpiniones.html.twig',['formOpinion' => $formOpinion->createView(),]);
    }

    /**
     * @Route("/opinion/listado", name="listado-opiniones")
     */
    public function listado(){
        // Consultando las opiniones de la BBDD, devuelve un array
        $opiniones = $this->getDoctrine()->getRepository(Opinion::class)->findAll();
                
        if (!$opiniones) {
            throw $this->createNotFoundException('No se han encontrado opiniones');
        }

        return $this->render('opinion/opiniones.html.twig', ['opiniones' => $opiniones]);
    }

    /**
    * @Route("/opinion/delete/{id}", name="eliminar-opinion")
    * @IsGranted("ROLE_ADMIN")
    */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $opinion = $entityManager->getRepository(Opinion::class)->find($id);

        if (!$opinion) {
            throw $this->createNotFoundException('No existe');
        }

        // Le indicamos a doctrine que nos gustaría eliminar este elemento, pero no hace la query
        $entityManager->remove($opinion);
        // Ejecuta la query
        $entityManager->flush();

      
        return $this->redirectToRoute('listado-opiniones');
    }
}
