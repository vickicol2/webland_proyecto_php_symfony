window.onload = function () {
    const solicitudSuccess = document.getElementsByClassName("form__mensaje--success")[0].innerHTML;
    if (solicitudSuccess != "") {
        alertify.notify(solicitudSuccess, "success", 8);
    }

    const solicitudError = document.getElementsByClassName("form__mensaje--error")[0].innerHTML;
    if (solicitudError != "") {
        alertify.notify(solicitudError, "error", 8);
    }

    // document.getElementById("btonNext").addEventListener("click", nextOpinion);
    // document.getElementById("btonBack").addEventListener("click", backOpinion);
};

function nextOpinion() {
    const divContainer = document.getElementsByClassName("opinions__container")[0];
    // When accessing data attributes from JavaScript, the attribute names are converted from dash-style to camelCase.
    // For example, data-is-authenticated becomes isAuthenticated and data-number-of-reviews becomes numberOfReviews.
    var pos = divContainer.dataset.posicionActual;
    console.log(`posicion Actual ${pos}`);

    var homepage = divContainer.dataset.url;
    console.log(homepage);

    /* $.ajax({
        url:   homepage,
        data:  {posicionActual: 9},
        dataType: "json",
        method:  'POST',
        headers: {
            // "My-First-Header":"X-Requested-With",
            "X-Requested-With":"X-Requested-With"
        },
        success:  function () {
            // console.log();
        }
        });
    */

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
        console.log("exito");
        }
    };
    xhttp.open("POST", homepage, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhttp.send("posicionActual=" + 45);
}

