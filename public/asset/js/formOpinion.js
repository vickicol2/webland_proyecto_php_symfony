window.onload = function() {
    
    const solicitudSuccess = document.getElementsByClassName('form__mensaje--success')[0].innerHTML;
    if (solicitudSuccess != ''){
        alertify.notify(solicitudSuccess, 'success', 8);
    }

    const solicitudError = document.getElementsByClassName('form__mensaje--error')[0].innerHTML;
    if (solicitudError != ''){
        alertify.notify(solicitudError, 'error', 8);
    }
};

